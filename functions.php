<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css', array(  ) );
    }
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css', 10 );

// END ENQUEUE PARENT ACTION

//Remove WP News
add_action('wp_dashboard_setup', 'wf_remove_dashboard_widgets');
function wf_remove_dashboard_widgets () {
	remove_meta_box('dashboard_primary', 'dashboard', 'side');// Remove WordPress Events and News
}

// Add Logo to backend
function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url('/wp-content/uploads/2018/08/Logo-1.png');
			height:auto;
			min-height:65px;
			width: 100%;
			background-size: contain;
			background-repeat: no-repeat;
			background-repeat: no-repeat;
			padding-bottom: 16.2%;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

//Misc admin CSS
function hide_woosidebars_columns(){ ?>
    <style type="text/css">
        .column-woosidebars_enable, .woosidebars_enable {
            display:none;
        }
    </style>
<?php }
add_action('admin_enqueue_scripts', 'hide_woosidebars_columns');

function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
    return get_bloginfo('name');
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

//Change "Howdy" Text
function howdy_message($translated_text, $text, $domain) {
	$new_message = str_replace('Howdy', 'Welcome', $text);
	return $new_message;
}
add_filter('gettext', 'howdy_message', 10, 3);

//Redirect Church Portal page depending
add_action('template_redirect', 'wf_church_portal_redirect');

function wf_church_portal_redirect(){
    if (is_page('church-supplies-portal') && !is_user_logged_in() ) {
        wp_redirect(wp_login_url());
        die;
    }
}
function wf_login_redirect( $redirect_to, $request, $user ) {
    //is there a user to check?
    if (isset($user->roles) && is_array($user->roles)) {
        //check for Churches
        if (in_array('church', $user->roles)) {
            // redirect them to user dashboard
            $redirect_to =  '/church-supplies-portal';
        }
    }

    return $redirect_to;
}

add_filter( 'login_redirect', 'wf_login_redirect', 10, 3 );

add_filter( 'et_project_posttype_args', 'mytheme_et_project_posttype_args', 10, 1 );
function mytheme_et_project_posttype_args( $args ) {
	return array_merge( $args, array(
		'public'              => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => false,
		'show_in_nav_menus'   => false,
		'show_ui'             => false
	));
}

//Remove Subscriber user role
add_action('admin_menu', 'remove_built_in_roles');
 
function remove_built_in_roles() {
    global $wp_roles;
 
    $roles_to_remove = array('subscriber');
 
    foreach ($roles_to_remove as $role) {
        if (isset($wp_roles->roles[$role])) {
            $wp_roles->remove_role($role);
        }
    }
}

//Hide admin bar for Churches
add_action('after_setup_theme', 'remove_admin_bar');
 
function remove_admin_bar() {
    $user = wp_get_current_user();
    if ( in_array( 'church', (array) $user->roles ) ) {
        show_admin_bar(false);
    }
}

//Remove Yoast Home URL
function wpseo_remove_home_breadcrumb($links)
{
	if ($links[0]['url'] == trailingslashit(get_home_url())) { 
        $links[0]['url'] = get_permalink( woocommerce_get_page_id( 'shop' ) );
        $links[0]['text'] = 'Shop';
        return $links;	
    }
}
add_filter('wpseo_breadcrumb_links', 'wpseo_remove_home_breadcrumb');

// Setup WooCommerce stuff

// Change divi show cart
function et_show_cart_total( $args = array() ) {
    if ( ! class_exists( 'woocommerce' ) || ! WC()->cart ) {
        return;
    }

    $defaults = array(
        'no_text' => false,
    );

    $args = wp_parse_args( $args, $defaults );

    $items_number = WC()->cart->get_cart_contents_count();

    $url = function_exists( 'wc_get_cart_url' ) ? wc_get_cart_url() : WC()->cart->get_cart_url();


    echo '<a href="/my-account/" class="my-account">My Account</a>';

    printf(
        '<a href="%1$s" class="et-cart-info">
            <span>View Cart (%2$s)</span>
        </a>',
        esc_url( $url ),
        ( ! $args['no_text']
            ? esc_html( sprintf(
                _nx( '%1$s Item', '%1$s Items', $items_number, 'WooCommerce items number', 'Divi' ),
                number_format_i18n( $items_number )
            ) )
            : ''
        )
    );
}
include_once(get_stylesheet_directory() . '/ecommerce/ecommerce.php');

//Create Options page for header
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Extra Ecommerce Settings',
		'menu_title'	=> 'Extra Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
}

//Fix Divi Ajax Cart issue
add_filter( 'woocommerce_add_to_cart_fragments', 'divi_ajax_cart_fix', 10, 1 ); 

function divi_ajax_cart_fix( $fragments ) {
    global $woocommerce;
	$fragments['.et-cart-info'] = '<a href="' . $woocommerce->cart->get_cart_url() . '" class="et-cart-info">
            <span>View Cart (' . WC()->cart->get_cart_contents_count() . ' Items)</span>
        </a>'; 
    return $fragments;
}

