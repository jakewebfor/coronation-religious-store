<?php


//Overwrite Divi stuff since we edited the product page a bunch anyway
// add wrapper so we can clear things
function unhook_divi_actions(){
    remove_action( 'woocommerce_before_single_product_summary', 'et_divi_output_product_wrapper', 0  );
    remove_action( 'woocommerce_after_single_product_summary', 'et_divi_output_product_wrapper_end', 0  );
}
add_action('wp_loaded', 'unhook_divi_actions');

//Generate left container for single product
// add_action( 'woocommerce_before_single_product_summary', 'create_left_area', 5 );
 
// function create_left_area() {
//     echo '<div id="left-area">';
// }

// add_action( 'woocommerce_after_single_product_summary', 'close_left_area', 5 );
 
// function close_left_area() {
//     echo '</div>';
//     echo '<div id="sidebar">'; 
//     dynamic_sidebar('sidebar-1');
//     echo '</div>';
// }

//Move sale text
remove_action('woocommerce_single_product_summary','woocommerce_template_single_title',5);
add_action('woocommerce_single_product_summary', 'woocommerce_my_single_title',5);

if ( ! function_exists( 'woocommerce_my_single_title' ) ) {
   function woocommerce_my_single_title() {
       global $product;
       if ($product->is_on_sale()){
           echo '<span class="onsale coronation-on-sale">Sale</span>';
       }
?>  
            <h1 itemprop="name" class="product_title entry-title"><span><?php the_title(); ?></span></h1>
<?php
    }
}

add_action( 'woocommerce_before_add_to_cart_button', 'add_quantity_label', 5 );
 
function add_quantity_label() {
    echo '<span class="quantity-label">Quantity</span>';
}