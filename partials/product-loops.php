<?php 
//Add "Browse Category" to loop items
function create_category_link( $mark_class_count_category_count_mark, $category ) { 
    $category_link = '<a class="browse-category" href="'. get_term_link($category->term_id) . '">Browse Category ></a>';
    $mark_class_count_category_count_mark .= $category_link;
    return $mark_class_count_category_count_mark;
}; 
         
// add the filter 
add_filter( 'woocommerce_subcategory_count_html', 'create_category_link', 10, 2 ); 