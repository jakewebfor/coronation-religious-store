<?php 

//Conditional tags

// Add "Add to Cart" buttons in Divi shop pages
add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 20 );

// Change "Sale!" text
add_filter( 'woocommerce_sale_flash', 'wc_custom_replace_sale_text' );
function wc_custom_replace_sale_text( $html ) {
    return str_replace( __( 'Sale!', 'woocommerce' ), __( 'Sale', 'woocommerce' ), $html );
}

//overwrite how Divi handles WooCommerce layout
function wf_overwrite_divi_woocommerce_header(){
  remove_action( 'woocommerce_before_main_content', 'et_divi_output_content_wrapper', 10 );
}

add_action( 'wp_loaded', 'wf_overwrite_divi_woocommerce_header' );

// Add Product header image if it exists
add_action( 'woocommerce_before_main_content', 'setup_header', 18 );

//Ovewrite Divi's Content Wrapper for Custom Field
function setup_header() {
  if (function_exists('get_field')){
      if (get_field('product_header_image', 'option')) : ?>
          <?php 
              $header_content = '<div class="product-header-container" style="background-image:url(' . get_field('product_header_image', 'option') . ')"></div>';
          ?>
      <?php endif ;
  } 
  echo '
      <div id="main-content">' . 
          $header_content .
    '<div class="container">
      <div id="content-area" class="clearfix">';
}

//Override WooCommerce breadcrumbs with yoast
remove_action( 'init', 'woocommerce_breadcrumb', 20 );
remove_action( 'woocommerce_before_main_content','woocommerce_breadcrumb', 20 );
//Add Yoast Breadcrumbs
add_action( 'woocommerce_before_main_content','my_yoast_breadcrumb', 20, 0 );
if (!function_exists('my_yoast_breadcrumb') ) {
  function my_yoast_breadcrumb() {
    yoast_breadcrumb('<p id="breadcrumbs">','</p>');
  }
}

// Add Product Category Header if it exists
function product_category_header(){
  if (is_product_category() && function_exists('get_field')){
    if (get_field('product_category_header', 'option')) {
      echo '<div class="product-category-header">';
      the_field('product_category_header', 'option');
      echo '</div>';
    }
    else {
      echo "showing up";
    }
  }
}

add_action( 'woocommerce_before_main_content','product_category_header', 21, 0 );

//Finally, create the Left area
function et_woocommerce_open_left_area(){
  echo '<div id="left-area">';
}

add_action( 'woocommerce_before_main_content','et_woocommerce_open_left_area', 22, 0 );

//Add "View Product Details"
function add_view_product_details($button, $product){
  if (is_product_category()){
    $button .= '<a class="product-learn-more" href="' . $product->get_permalink() . '">View Product Details ></a>';
    return $button;
  }

}

add_filter('woocommerce_loop_add_to_cart_link', 'add_view_product_details', 10, 2);

//Get Ecommerce partials
function load_ecommerce_partials() {

  require_once(get_stylesheet_directory() . '/partials/single-product.php');

  require_once(get_stylesheet_directory() . '/partials/product-loops.php');
}

add_action('init', 'load_ecommerce_partials');